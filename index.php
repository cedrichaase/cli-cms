<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<title>c20e</title>
<style>
    @keyframes flash1 {
        0% { opacity: 1; }
        50% { opacity: .1; }
        100% { opacity: 1; }
    }

    @keyframes flash2 {
        0% { opacity: 1; }
        50% { opacity: .1; }
        100% { opacity: 1; }
    }

    html {
        background: url('/bg.jpg') no-repeat center center fixed;
        background-size: cover;

        height: 100%;
        width: 100%;

        padding: 0;
        margin: 0;
    }

    body {
        --inner-pad: 2rem;

        margin: 0;
        padding: var(--inner-pad);

        min-height: calc(100vh - 2 * var(--inner-pad));

        background-color: rgba(0, 0, 0, 0.8);
        color: lightgrey;
        font-family: Monaco, Monospace, sans-serif;
    }

    pre {
        font-family: Monaco, Monospace;
        font-size: 9 !important;
    }

    strong {
        color: white;
    }


    p {
        margin: 0;
        padding: 0;
        font-size: 9 !important;

        word-break: break-all;
        word-wrap: break-word;
    }

    a {
        font-weight: bold;
        color: #24826c;
    }

    .cursor {
        background: lightgrey;
        color: white;
        height: 15px;
    }

    .cm {
        color: #24826c;
    }

    .hostname {
        color: #24826c;
    }

    .stdin {
        whitespace: pre-line;
    }
</style>
<script charset="UTF-8">
    const ls = '<?php $out = []; exec('ls -l /var/www/html/txt', $out); echo implode('<br/>', $out); ?>'

    const prompt = `<p class="prompt active">[<span class="username">guest</span>@<span class="hostname">c20e.de</span> ~ ]$ <span class="stdin"></span><span class="cursor">&nbsp;</span></p>`
    
    const termElem = () => document.querySelector('main');
    const activePromptElem = () => document.querySelector('.prompt.active');

    const HISTORY = [];

    const write = str => termElem().innerHTML += str
    const writep = str => write(`<p>${str}</p>`)
        
    const commands = new Map([
        ['help', async (argv) => {
            await evaluate('cat help');
        }],
        ['whoami', () => {
            writep('guest');
        }],
        ['clear', () => {
            termElem().innerHTML = '';
        }],
        ['ls', (argv) => {
            if (argv[1] === '/bin') {
                const cmds = [...commands.keys()].join(' ');
                writep(`<span style="color: lightgreen;">${cmds}</span>`);
                return;
            }

            writep(ls);
        }],
        ['cat', async (argv) => {
            let result = '';

            for (const file of argv.slice(1)) {
                const resp = await fetch(`/txt/${file}`);

                if (resp.ok) {
                    const content = await resp.text();

                    result += `${content}`;

                    continue;
                }

                result += `cat: ${file}: No such file or directory<br/>`;
            }

            write(`<pre>${result}</pre>`);
        }],
        ['echo', (argv) => {
            const arg = argv.slice(1).join(' ')
            writep(arg.trim().replace(/\'/gi, '').replace(/\"/gi, ''));
        }],
        ['pwd', () => writep('/home/guest')],
        ['file', async (argv) => {
            let result = '';

            for (const file of argv.slice(1)) {
                const resp = await fetch(`/txt/${file}`);

                if (resp.ok) {
                    result += `${file}: HTML document, ASCII text<br/>`;
                    continue;
                }

                result += `${file}: cannot open '${file}' (No such file or directory)<br/>`;
            }

            writep(result);
        }],
        ['which', (argv) => {
            let result = '';
            
            for (const program of argv.slice(1)) {
                if (!commands.has(program)) {
                    continue;
                }

                result += `/bin/${program}<br/>`;
            }

            writep(result);
        }],
        ['uname', (argv) => {
            writep('Linux');
        }]
    ]);

    async function appendPrompt() {
        const prompts = document.querySelectorAll('.prompt');
        
        // evaluate cmd and deactivate previous prompt
        if (prompts.length) {
            const prev = prompts.item(prompts.length - 1);

            prev.classList.remove('active');
            prev.querySelector('.cursor').style.display = 'none';

            const cmd = prev.querySelector('.stdin').innerHTML.trim();

            if (cmd) {
                await evaluate(cmd);
            }
        }
        
        write(prompt);
        window.scrollTo(0, termElem().scrollHeight);
    }

    async function evaluate(cmd) {
        const argv = cmd.split(' ');
        const handler = commands.get(argv[0]);
        
        HISTORY.push(cmd);

        if (handler) {
            return handler(argv);
        }

        writep(`wsh: command not found: ${argv[0]}`);
    }

    setTimeout(async () => {
        await evaluate('cat .wsh_profile.php');
        HISTORY.pop();
        await appendPrompt();
    })

    let historyPtr = 0;

    window.onkeydown = ev => {
        if (ev.key.startsWith('Arrow') || ev.key === 'Tab') {
            ev.preventDefault();
            return false;
        }

        return true;
    }

    window.onkeyup = async (ev) => {
        ev.preventDefault();

        const key = ev.key
        const stdin = document.querySelector('.prompt.active >.stdin')
        const cursor = document.querySelector('.prompt.active > .cursor')
        
        if (key === 'Enter') {
            await appendPrompt(); 
            historyPtr = HISTORY.length - 1;
            return;
        }

        if (key === 'Backspace') {
            stdin.innerHTML = stdin.innerHTML.slice(0, -1); return;
        }

        if (key === 'ArrowUp') {
            stdin.innerHTML = (HISTORY[historyPtr] || '');
            if (historyPtr > 0) historyPtr--;
            return;
        }

        if (key === 'ArrowDown') {
            historyPtr = Math.min(HISTORY.length, historyPtr + 1);
            stdin.innerHTML = (HISTORY[historyPtr] || '');
            return;
        }

        if (key === 'Tab') {
            const matches = Array.from(commands.keys()).filter(c => c.startsWith(stdin.innerHTML));

            if (matches.length === 1 && stdin.innerHTML !== matches[0]) {
                stdin.innerHTML = matches[0];
                return;
            }

            cursor.style.animation = cursor.style.animation.includes('flash1') ? 'flash2 linear 100ms' : 'flash1 linear 100ms';
            return;
        }

        if (ev.ctrlKey) {
            if (['w', 'W'].includes(key)) {
                stdin.innerHTML = stdin.innerHTML.trim().split(' ').slice(0, -1).join(' ') + ' ';
                return;
            }

            if (['l', 'L'].includes(key)) {
                await evaluate('clear');
                await appendPrompt();
            }        
        }

        if (key.length !== 1) {
            return;
        }
        
        stdin.innerHTML += key
    }
</script>
</head>
<body>
<main></main>
</body>
</html>
